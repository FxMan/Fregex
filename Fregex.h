/*
 *
	
 *
 *
 *
 * Supports:
 * ---------
 *   '.'        Dot, matches any character
 *   '^'        Start anchor, matches beginning of string
 *   '$'        End anchor, matches end of string
 *   '*'        Asterisk, match zero or more (greedy)
 *   '+'        Plus, match one or more (greedy) 
 *   '?'        Question, match zero or one (non-greedy) 问号表达式存在BUG
 *   '[abc]'    Character class, match if one of {'a', 'b', 'c'}
 *   '[^abc]'   Inverted class, match if NOT one of {'a', 'b', 'c'} -- NOTE: feature is currently broken!
 *   '[a-zA-Z]' Character ranges, the character set of the ranges { a-z | A-Z }
 *   '\s'       Whitespace, \t \f \r \n \v and spaces
 *   '\S'       Non-whitespace
 *   '\w'       Alphanumeric, [a-zA-Z0-9_]
 *   '\W'       Non-alphanumeric
 *   '\d'       Digits, [0-9]
 *   '\D'       Non-digits
 *
ASCII码表具体如下所示:
(二进制)	(八进制)	(十进制)	(十六进制)			字符名称
 0000				040				32				  0x20				(space)	空格
 0001				041				33				  0x21				!	叹号
 0010				042				34				  0x22				"	双引号
 0011				043				35				  0x23				#	井号
 0100				044				36				  0x24				$	美元符
 0101				045				37				  0x25				%	百分号
 0110				046				38				  0x26				&	和号
 0111				047				39				  0x27				'	闭单引号
 1000				050				40				  0x28				(	开括号
 1001				051				41				  0x29				)	闭括号
 1010				052				42				  0x2A				*	星号
 1011				053				43				  0x2B				+	加号
 1100				054				44				  0x2C				,	逗号
 1101				055				45				  0x2D				-	减号/破折号
 1110				056				46				  0x2E				.	句号
 1111				057				47				  0x2F				/	斜杠
 0000				060				48				  0x30				0	字符0
 0001				061				49				  0x31				1	字符1
 0010				062				50				  0x32				2	字符2
 0011				063				51				  0x33				3	字符3
 0100				064				52				  0x34				4	字符4
 0101				065				53				  0x35				5	字符5
 0110				066				54				  0x36				6	字符6
 0111				067				55				  0x37				7	字符7
 1000				070				56				  0x38				8	字符8
 1001				071				57				  0x39				9	字符9
 1010				072				58				  0x3A				:	冒号
 1011				073				59				  0x3B				;	分号
 1100				074				60				  0x3C				<	小于
 1101				075				61				  0x3D				=	等号
 1110				076				62				  0x3E				>	大于
 1111				077				63				  0x3F				?	问号
 0000				0100			64				  0x40				@	电子邮件符号
 0001				0101			65				  0x41				A	大写字母A
 0010				0102			66				  0x42				B	大写字母B
 0011				0103			67				  0x43				C	大写字母C
 0100				0104			68				  0x44				D	大写字母D
 0101				0105			69				  0x45				E	大写字母E
 0110				0106			70				  0x46				F	大写字母F
 0111				0107			71				  0x47				G	大写字母G
 1000				0110			72				  0x48				H	大写字母H
 1001				0111			73				  0x49				I	大写字母I
1010				0112			74				  0x4A				J	大写字母J
 1011				0113			75				  0x4B				K	大写字母K
 1100				0114			76				  0x4C				L	大写字母L
 1101				0115			77				  0x4D				M	大写字母M
 1110				0116			78				  0x4E				N	大写字母N
 1111				0117			79				  0x4F				O	大写字母O
 0000				0120			80				  0x50				P	大写字母P
 0001				0121			81				  0x51				Q	大写字母Q
 0010				0122			82				  0x52				R	大写字母R
 0011				0123			83				  0x53				S	大写字母S
 0100				0124			84				  0x54				T	大写字母T
 0101				0125			85				  0x55				U	大写字母U
 0110				0126			86				  0x56				V	大写字母V
 0111				0127			87				  0x57				W	大写字母W
 1000				0130			88				  0x58				X	大写字母X
 1001				0131			89				  0x59				Y	大写字母Y
 1010				0132			90				  0x5A				Z	大写字母Z
 1011				0133			91				  0x5B				[	开方括号
 1100				0134			92				  0x5C				\	反斜杠
 1101				0135			93				  0x5D				]	闭方括号
 1110				0136			94				  0x5E				^	脱字符
 1111				0137			95				  0x5F				_	下划线
 0000				0140			96				  0x60				`	开单引号
 0001				0141			97				  0x61				a	小写字母a
 0010				0142			98				  0x62				b	小写字母b
 0011				0143			99				  0x63				c	小写字母c
 0100				0144			100				  0x64				d	小写字母d
 0101				0145			101				  0x65				e	小写字母e
 0110				0146			102				  0x66				f	小写字母f
 0111				0147			103				  0x67				g	小写字母g
 1000				0150			104				  0x68				h	小写字母h
 1001				0151			105				  0x69				i	小写字母i
 1010				0152			106				  0x6A				j	小写字母j
 1011				0153			107				  0x6B				k	小写字母k
 1100				0154			108				  0x6C				l	小写字母l
 1101				0155			109				  0x6D				m	小写字母m
 1110				0156			110				  0x6E				n	小写字母n
 1111				0157			111				  0x6F				o	小写字母o
 0000				0160			112				  0x70				p	小写字母p
 0001				0161			113				  0x71				q	小写字母q
 0010				0162			114				  0x72				r	小写字母r
 0011				0163			115				  0x73				s	小写字母s
 0100				0164			116				  0x74				t	小写字母t
 0101				0165			117				  0x75				u	小写字母u
 0110				0166			118				  0x76				v	小写字母v
 0111				0167			119				  0x77				w	小写字母w
 1000				0170			120				  0x78				x	小写字母x
 1001				0171			121				  0x79				y	小写字母y
 1010				0172			122				  0x7A				z	小写字母z
 1011				0173			123				  0x7B				{	开花括号
 1100				0174			124				  0x7C				|	垂线
 1101				0175			125				  0x7D				}	闭花括号
 1110				0176			126				  0x7E				~	波浪号
 1111				0177			127				  0x7F				DEL (delete)	删除
 *
 */
#ifndef __fregex_H
#define __fregex_H
#ifdef __cplusplus
extern "C"{
#endif
#include <stdbool.h>


/* Typedef'd pointer to get abstract datatype. */
typedef struct regex_t* re_t;


/* Compile regex string pattern to a regex_t-array. */
re_t re_compile(const char* pattern);


/* Find matches of the compiled pattern inside text. */
int  re_matchp(re_t pattern, const char* text);


/* Find matches of the txt pattern inside text (will compile automatically first). */
int  re_match(const char* pattern, const char* text);

//src 被搜索的字符串 reg 正则表达式 s,匹配字符串的偏移 l匹配字符串的长度
bool re_find(const char* src, const char* reg, int* s, int* l);


#ifdef __cplusplus
}
#endif
#endif